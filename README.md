# README #

### What is this repository for? ###

* Newspaper Delivery System
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone Repo into Eclipse - Using you're Branch
* When ready and code is approved commit to development Branch
* Test Branch is for testing do not commit and push to master - use local Repo
* New feature branch is for features we want to add. 
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests  J-unit
* Code review group Review

