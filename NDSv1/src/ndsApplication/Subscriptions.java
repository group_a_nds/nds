package ndsApplication;

public class Subscriptions {
	private String subID;
	private String subFreq;
	 
	/**
	* Subscriptions Class
	*/
		
		
		public Subscriptions( String subID,String subFreq ) 
		{
			this.subID=subID;
			this.subFreq=subFreq;
		}
		public Subscriptions() {
			// TODO Auto-generated constructor stub
		}
		public String getSubID() 
		{
			return subID;
		}

		public void setSubID(String subID) 
		{
			this.subID = subID;
		}

		public String getSubFreq() 
		{
			return subFreq;
		}
		
		public void setSubFreq(String subFreq)
		{
			this.subFreq = subFreq;
		}

}
