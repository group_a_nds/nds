package ndsApplication;
import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class DatabaseHandler {
    Connection con;	
    Statement stmt;      
    ResultSet rs;	
    int count;
    int current;
    
    private static String freq;
	private static String pubName;
	private static String pubPrice;
	
	private static String subID;
	private static String subFreq;
    	
	
	public static String getSubID() {
		return subID;
	}
	public static String getSubFreq() {
		return subFreq;
	}
    
	public static String getFreq() {
		return freq;
	}

	public static String getPubName() {
		return pubName;
	}
	public static String getPubPrice() {
		return pubPrice;
	}

	public DatabaseHandler() {
		con = null;
        stmt = null;
        rs = null;
        count = 0;
        current = 0;
        
        dbConn();		// method to connect to database using odbc-jdbc
        //initDB();		// method to initialise gui with database info

      }

	 /**
	    *<h2>dbConn creates connection with the database</h2>
	    *<p>This method creates a connection with the database that the members 
	    *details are stored. This method uses SQL commands that are passed to
	    *the database. </p>
	    *
	    *@param url Holds the file path of the MS Access Database
	    *@param con Database connection
	    *@param stmt Used for executing database statements
	    *@param rs Holds result of stmt, moves to next row in database
	    *@param count Counts the number of lines in the database
	 */
 private void dbConn()
{
  

	try		
	{	
			// driver to use with named database
		String url = "jdbc:ucanaccess://C:/Swing/NDSTest.accdb";
			// connection represents a session with a specific database
		con = DriverManager.getConnection(url);

			// stmt used for executing sql statements and obtaining results
		stmt = con.createStatement();

		rs = stmt.executeQuery("SELECT * FROM Customers");
		rs.next();
   
            
		/*while(rs.next())	// count number of rows in table
			count++;*/
		rs.close();
	}
	catch(Exception e) {
		
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
			    "ERROR Error in start up...... dbConn!",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
		}
}

 
 public boolean saveCustomerDetails(Customer customer) {
	
		try
		{

                         
			String newCust = "INSERT INTO Customers(custName, address, phone, email, areaCode)VALUES('"+customer.getName()+"', '"+customer.getAddress()+"', '"+customer.getPhone()+"', '"+customer.getEmail()+"', '"+customer.getAreaCode()+"')";
            stmt.executeUpdate(newCust);
                  
            Component frame = null;
			JOptionPane.showMessageDialog(frame,
					customer.getName() +" stored in database");
          
		}
		catch(SQLException e) {
			Component frame = null;
			JOptionPane.showMessageDialog(frame,
					"Error in creating new Member row in database",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			
             return false;
             }
	 
		return true;
	 
 }
 
 public Subscriptions searchSubByID(String id) throws SQLException {
		
		String SubIDDb,SubFreqeDb;
		Subscriptions searchSub = new Subscriptions();
		
		try {
		//
			
		String statement ="SELECT * FROM Subscriptons Where SubID = '"+id+"';" ;
	
		rs = stmt.executeQuery(statement);
			//rs.next();
		
		if(rs.next()) {
			
			searchSub.setSubID(SubIDDb = rs.getString("SubID"));
			searchSub.setSubFreq(SubFreqeDb = rs.getString("SubFrequency"));
			
		}else {
			
            Component frame = null;
			JOptionPane.showMessageDialog(frame,
					"ID: " + id  + " Not found!");
			
		}
	   
		} catch (SQLException e) {
			Component frame = null;
			JOptionPane.showMessageDialog(frame,
					"Error in Searching for subscription ID",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}
	rs.close();
	return searchSub;
	}



public boolean saveSubscriptions(String publication, String frequency, String name) throws SQLException {
	
    String freq = frequency;
	
	rs = stmt.executeQuery("SELECT custId FROM Customers Where custName = '"+name+"';");
	rs.next();
    String customerid = rs.getString("custId");
    
    
    rs = stmt.executeQuery("SELECT pubId FROM Publications Where PubName = '"+publication+"';");
	rs.next();
    String pubid = rs.getString("pubId");
   
    try
	{           
		String newSub = "INSERT INTO Subscriptions(custId, pubId, Frequency)VALUES('"+customerid+"', '"+pubid+"', '"+freq+"')";
		System.out.println(newSub);
                 stmt.executeUpdate(newSub);
                 
	}
	catch(SQLException e) {
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in creating new Member row in database",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
         return false;
         }
	
	
	return true;
	
}

public boolean saveStaffDetails(Staff staff) {
	 
	 try
		{     
			String newStaff = "INSERT INTO Staff(staffName)VALUES('"+staff.getName()+"')";
	
            stmt.executeUpdate(newStaff);
             
            Component frame = null;
 			JOptionPane.showMessageDialog(frame,
 					staff.getName() + "stored in database.");
		}
		catch(SQLException e) {
			
			Component frame = null;
			JOptionPane.showMessageDialog(frame,
					"Error in creating new Member row in database",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	 
	 return false;
	 
}
//Add new publications
public boolean savePublicationDetails(Publications publication) {
	
	try
	{

		String newPub = "INSERT INTO Publications(pubName, pubPrice)VALUES('"+publication.getPubName()+"', '"+publication.getPubPrice()+"')";
	
              stmt.executeUpdate(newPub);
              
              Component frame = null;
   			JOptionPane.showMessageDialog(frame,
   					publication.getPubName() +" stored in database");
	}
	catch(SQLException e) {
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in creating new Member row in database",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
	}
 
 return false;
 
}
//Search Publications
public Publications searchPubByName(String name) throws SQLException {
	
	String PubNameDb;
	double PubPriceDb;
	Publications searchPub = new Publications();
	
	try {
	//
		
	String statement ="SELECT * FROM Publications Where PubName = '"+name+"';" ;

	
	
	rs = stmt.executeQuery(statement);
		//rs.next();
	
	if(rs.next()) {
		
	
		searchPub.setPubName(PubNameDb = rs.getString("PubName"));
		searchPub.setPubPrice(PubPriceDb = rs.getDouble("PubPrice"));
		
	}else {
		 
		Component frame = null;
			JOptionPane.showMessageDialog(frame,
					name + " not found in database");
		
	}
   
	} catch (SQLException e) {
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in Searching for Publication",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
	}
rs.close();
return searchPub;
}

public Customer searchCustByName(String name) throws SQLException {

	String custNameDb=null, AddressDb, PhoneDb, EmailDb, areaCodeDb, BalanceDb;
	Customer searchCust = new Customer();
		try {
		//
			
		String statement ="SELECT * FROM Customers Where custName = '"+name+"';";
		
		rs = stmt.executeQuery(statement);
			//rs.next();
		
		if(rs.next()) {
			
			searchCust.setName(custNameDb = rs.getString("custName"));
			searchCust.setAddress(AddressDb = rs.getString("Address"));
			searchCust.setPhone(PhoneDb = rs.getString("Phone"));
			searchCust.setEmail(EmailDb = rs.getString("Email"));
			searchCust.setAreaCode(areaCodeDb = rs.getString("areaCode"));
			searchCust.setBalance(BalanceDb = rs.getString("Balance"));
			
			rs.close();
			
		}
		
		 rs = stmt.executeQuery("SELECT custId FROM Customers Where custName = '"+name+"';");
			rs.next();
		    String custId = rs.getString("custId");
		    rs.close();
		    
		 rs = stmt.executeQuery("SELECT pubId, Frequency FROM Subscriptions Where custId = '"+custId+"';");
			rs.next();
			   String pubId = rs.getString("pubId");
			   freq = rs.getString("Frequency");
			    rs.close();

		
				 rs = stmt.executeQuery("SELECT PubName FROM Publications Where pubId = '"+pubId+"';");
					rs.next(); 
					pubName = rs.getString("pubName");
				    rs.close();

		} catch (SQLException e) {
			
			if(custNameDb == null ) {
				//default title and icon
				Component frame=null;
				JOptionPane.showMessageDialog(frame,
				   name +  " not found in Database.");
			}
	rs.close();
	}
	return searchCust;
}

public Staff searchStaffByName(String name) throws SQLException {
	
	String staffNameDb;
	Staff searchStaff = new Staff();
	
	try {

		String staffQuery ="SELECT * FROM Staff Where staffName = '"+name+"';" ;
		
	
	rs = stmt.executeQuery(staffQuery);
		//rs.next();
	
	if(rs.next()) {
		
	
		searchStaff.setName(staffNameDb = rs.getString("staffName"));
	
	}
	
	} catch (SQLException e) {
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in searching for staff member",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
	}
rs.close();
return searchStaff;
}

public void DelStaffByName(String name) throws SQLException {
	
	try
	{   

		String archiveStaff = "INSERT INTO StaffArchive(StaffName)VALUES('"+name+"')";
		
        stmt.executeUpdate(archiveStaff);
        	
		String updateTemp ="DELETE FROM Staff WHERE staffName = '"+name+"';"; 
		stmt.executeUpdate(updateTemp);
		
		
	}catch (SQLException sqle)
	{	
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in deleting staff member from database",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
		
	}
	

}

public void archiveCustomer(String name) throws SQLException {
	
	try
	{   

		String archiveCustomer = "INSERT INTO CustArchive (custId, custName, Address, Phone, Email, areaCode, Balance) Select custId, custName, Address, Phone, Email, areaCode, Balance From Customers where custName = '"+name+"';";
		//add prompt
        stmt.execute(archiveCustomer);		
		
		String updateTemp ="DELETE FROM Customers WHERE custName = '"+name+"';"; 
		stmt.execute(updateTemp);
			
		
	}catch (SQLException sqle)
	{
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in Archive customer",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
		
	}
	

}

public ArrayList createDeliveryDocket(String code) throws SQLException {
	
	
	ArrayList al = new ArrayList();
	
	String cmd = "Select custName, Address, subId, PubName, areaCode from Customers, Subscriptions ,Publications  Where Customers.custId = Subscriptions.custId and Subscriptions.pubId = Publications.pubId and Frequency = '"+code+"' Order by Customers.areaCode;" ;
	
	rs = stmt.executeQuery(cmd);
	
	while(rs.next()) {
		DeliveryDocket createDocket = new DeliveryDocket();
		createDocket.name = rs.getString("custName");
		createDocket.address = rs.getString("Address");
		createDocket.subId = rs.getString("subId");
		createDocket.pName = rs.getString("PubName");
		createDocket.aCode = rs.getString("areaCode");
		
		al.add(createDocket);
		}rs.close();
		


		return al;
	}

public void updatePubDetails(String pubName, double pubPrice2) {
	
	
	String pName = pubName;
	double pPrice = pubPrice2;
	String pId = null;
	
	 	try {
			rs = stmt.executeQuery("SELECT pubId FROM Publications Where PubName = '"+pName+"';");
		rs.next();
	     pId = rs.getString("pubId");
	    rs.close();
	
		} catch (SQLException e) {
			Component frame = null;
			JOptionPane.showMessageDialog(frame,
					"Could not find publication details",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}
	 	
	 	
	 	String updatePub = ("UPDATE Publications SET " +  "PubPrice = '" +pPrice+ "' WHERE pubId = '" +pId+"';");
	 	
	 	try {
			 stmt.executeUpdate(updatePub);
			 Component frame = null;
			JOptionPane.showMessageDialog(frame,
					   pName +  " Price updated.");
			 
				
		} catch (SQLException e) {
			
			Component frame = null;
			JOptionPane.showMessageDialog(frame,
				    "ERROR could not update to price " + pPrice + "Please try again!",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	
}

public void DelPubByName(String name) {
	
	try
	{   

		String updateTemp ="DELETE FROM Publications WHERE PubName = '"+name+"';"; 
		stmt.executeUpdate(updateTemp);
		
		
	}catch (SQLException sqle)
	{
		Component frame = null;
		JOptionPane.showMessageDialog(frame,
				"Error in deleting publication from database",
			    "ERROR", JOptionPane.ERROR_MESSAGE);
		
	}

}

public void updateSubDetails(String name, String publication, String frequency) throws SQLException  {
	
		//Update subscriptions
	
	  	String freq = frequency;
		
		rs = stmt.executeQuery("SELECT custId FROM Customers Where custName = '"+name+"';");
		rs.next();
	    String customerid = rs.getString("custId");
	  
	    
	    rs = stmt.executeQuery("SELECT pubId FROM Publications Where PubName = '"+publication+"';");
		rs.next();
	    String pubid = rs.getString("pubId");
	  
	    
	    rs = stmt.executeQuery("SELECT subId FROM Subscriptions Where custId = '"+customerid+"';");
	  	rs.next();
	  	String subId = rs.getString("subId");
	

	    try
		{           
			String updatefreq = ("UPDATE Subscriptions SET " +  "Frequency = '" +freq+ "' WHERE subId = '" +subId+"';");
			
	        stmt.executeUpdate(updatefreq);
	                 
	                 
	        String updatepub = ("UPDATE Subscriptions SET " +  "pubId = '" +pubid+ "' WHERE subId = '" +subId+"';");
	        
	     	 stmt.executeUpdate(updatepub);

	     	   Component frame = null;
				JOptionPane.showMessageDialog(frame,
						"Subscription updated in database");
	
		}
		catch(SQLException e) {
			Component frame = null;
			JOptionPane.showMessageDialog(frame,
					"Error in updating subscriptions in database",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
	         }

}
public ArrayList<Invoice> showInvoice(String area) throws SQLException {
	
	ArrayList<Invoice> bill = new ArrayList<Invoice>();
	
	String cmd = "Select custId, custName, Address, Phone, PubName, PubPrice, areaCode, Balance, Frequency from Customers, Subscriptions ,Publications  Where Customers.custId = Subscriptions.custId and Subscriptions.pubId = Publications.pubId and AreaCode = '"+area+"' Order by Customers.areaCode;" ;
		
	rs = stmt.executeQuery(cmd);	
	
	while(rs.next()) {
		Invoice bills = new Invoice();
		bills.id = rs.getString("custId");
		bills.name = rs.getString("custName");
		bills.address = rs.getString("Address");
		bills.iphone = rs.getString("Phone");
		bills.areaCode = rs.getString("areaCode");
		bills.bal= rs.getInt("Balance");
		bills.frequency = rs.getString("Frequency");
		bills.pubName = rs.getString("pubName");
	    bills.pubPrice = rs.getInt("PubPrice");
	    
		
	    bill.add(bills);	
	}
	     
	 rs.close();
     
return bill;
}


}

