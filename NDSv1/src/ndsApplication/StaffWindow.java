package ndsApplication;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ExceptionHandlers.StaffNameExceptionHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.awt.Color;

public class StaffWindow extends JFrame {

	private JPanel contentPane;
	private JTextField StaffNameField;
	private String name;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StaffWindow frame = new StaffWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public boolean validateAplhaChar(String Name) throws StaffNameExceptionHandler{
		
		boolean result = false;
		
		if(!Name.matches("[a-z A-Z]+")){
			JOptionPane.showMessageDialog(contentPane,
					"Name: " + Name + " - is not a valid name it must only be Alphabetic characters",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
		
			throw new StaffNameExceptionHandler("Must be Alphabetic Characters");
			
		
	}
		
		return result;
		
	}
	
	public boolean validateNameLength(String Name) throws StaffNameExceptionHandler {
		// validateNameLenggth returns true if the Name is between 3 and 16 Characters
		
		boolean result = false;
		
		if (Name.length() >= 1 && Name.length() <=3) {
			JOptionPane.showMessageDialog(contentPane,
					"Name: " + Name + " - Must be greater than 3 letters",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			
			throw new StaffNameExceptionHandler("Length less than 3");
			
		}
		else if (Name.length() >= 6  && Name.length() <=15) {
				
			result = true;
			System.out.println(Name + " is a valid name");
		
		}else if (Name.length() >= 25 && Name.length() <= 100) {
			JOptionPane.showMessageDialog(contentPane,
					"Name: " + Name + " - Must be less than 25 letters",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			throw new StaffNameExceptionHandler("Length greater than 25");
		}
		
		
		return result;
	}

	/**
	 * Create the frame.
	 */
	public StaffWindow() {
		setTitle("Staff Details");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		     
		        	dispose();
		        }
		    }
		);
	
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		StaffNameField = new JTextField();
		StaffNameField.setColumns(10);
		StaffNameField.setBounds(208, 39, 203, 20);
		contentPane.add(StaffNameField);
		
		JLabel lblStaffMemberName = new JLabel("Staff Member Name");
		lblStaffMemberName.setBounds(71, 42, 127, 20);
		contentPane.add(lblStaffMemberName);
		
		JButton BtnClear = new JButton("CLEAR");
		BtnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				StaffNameField.setText(null);
			}
		});
		BtnClear.setBounds(75, 149, 104, 23);
		contentPane.add(BtnClear);
		
		JButton BtnSave = new JButton("SAVE");
		BtnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				name = StaffNameField.getText();
			
						try {
							validateAplhaChar(name);
							validateNameLength(name);
						
						Staff staff = new Staff(name);
	
						DatabaseHandler dbHandler = new DatabaseHandler();
						dbHandler.saveStaffDetails(staff);
						
						//default title and icon
						JOptionPane.showMessageDialog(contentPane,
						   name +  " Stored in Database.");
						
						StaffNameField.setText(null);
							
						} catch (StaffNameExceptionHandler e1) {
							
							
							StaffNameField.setText(null);
							StaffNameField.requestFocus();
						}
					
			  }
			
		});
		BtnSave.setBounds(254, 227, 104, 23);
		contentPane.add(BtnSave);
		
		JButton BtnDelete = new JButton("Delete");
		BtnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				name = StaffNameField.getText();
				
				DatabaseHandler dbHandler = new DatabaseHandler();
				
				Staff staffName = null;
			
				try {
					staffName = dbHandler.searchStaffByName(name);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				StaffNameField.setText(staffName.getName());
				
				if(staffName.getName() == null ) {
					//default title and icon
					JOptionPane.showMessageDialog(contentPane,
					   name +  " not found in Database.");
					
				}else {
				
				//default icon, custom title
				int n = JOptionPane.showConfirmDialog(
				    contentPane,
				    "Are you sure you would like to delete " + name + "?" ,
				    "Confirm",
				    JOptionPane.YES_NO_OPTION);
				
				StaffNameField.setText(null);
				
				if(n == JOptionPane.YES_OPTION) {
			
				try {
					dbHandler.DelStaffByName(name);
					
					//default title and icon
					JOptionPane.showMessageDialog(contentPane,
					   name +  " Deleted from Database.");
					StaffNameField.setText(null);
				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}}
			}
		});
		BtnDelete.setForeground(Color.RED);
		BtnDelete.setBounds(75, 227, 104, 23);
		contentPane.add(BtnDelete);
		
		JButton BtnSearch = new JButton("Search");
		BtnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				name = StaffNameField.getText();
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
					Staff staffName = dbHandler.searchStaffByName(name);
					
					StaffNameField.setText(staffName.getName());
					
					if(staffName.getName() == null ) {
						//default title and icon
						JOptionPane.showMessageDialog(contentPane,
						   name +  " not found in Database.");
						
					}else
					
					//default title and icon
					JOptionPane.showMessageDialog(contentPane,
					   staffName.getName() +  " found in Database.");
				
					
				} catch (SQLException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		BtnSearch.setBounds(254, 149, 104, 23);
		contentPane.add(BtnSearch);
	}
}
