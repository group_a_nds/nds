package ndsApplication;

public class Customer {

	private String phone,name, address, email,areaCode;
	private String balance;
	//Constructor
	public Customer( String name, String addr, String phone, String email, String areaCode) {
		this.name=name;
		this.address=addr;
		this.phone=phone;
		this.email=email;
		this.areaCode=areaCode;
	}

	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String string) {
		this.areaCode = string;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String string) {
		this.balance = string;
	}

}
