package ndsApplication;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.*; 

class WelcomeWindow extends JFrame implements ActionListener{
	
	JFrame frame;
	private JButton newsAgentButton=new JButton("News Agent");
	private JButton deliveryDriverButton=new JButton("Delivery Driver");
	private JButton exitButton=new JButton("Exit");
	private final JLabel LogoLabel = new JLabel("");
		

public static void main(String[] args) {
		
		WelcomeWindow window= new WelcomeWindow("Welcome");
		//window.setLocationRelativeTo(null);
	
}



//CONSTRUCTOR
	public WelcomeWindow(String s){
		
		super(s);
		
		getContentPane().setBackground(SystemColor.menu);
		
		Container content=getContentPane();
		
		getContentPane().setLayout(null);
		newsAgentButton.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		newsAgentButton.setBounds(10, 220, 163, 53);
		getContentPane().add(newsAgentButton);
		deliveryDriverButton.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		deliveryDriverButton.setBounds(183, 220, 163, 53);
		getContentPane().add(deliveryDriverButton);
		exitButton.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		exitButton.setBounds(356, 220, 163, 53);
		getContentPane().add(exitButton);
		LogoLabel.setBackground(UIManager.getColor("Button.background"));
//Logo		
		LogoLabel.setBounds(32, 11, 471, 183);
		getContentPane().add(LogoLabel);
        Image img = new ImageIcon(this.getClass().getResource("/NDS.png")).getImage();
	    LogoLabel.setIcon(new ImageIcon(img));
		
		
		exitButton.addActionListener(this);
		deliveryDriverButton.addActionListener(this);		
		newsAgentButton.addActionListener(this);
		setSize(546,322);    setVisible(true);}

// Buttons Action Performed
	public void actionPerformed(ActionEvent e){
				Object target=e.getSource();
		if (target==newsAgentButton)
		{
			NewsAgentLogin window = new NewsAgentLogin("NewsAgent Login");
			window.setLocationRelativeTo(null);
			window.setVisible(true);
			setVisible(false);//closes window
		}
		
	if (target==deliveryDriverButton)
		{
		DeliveryDriverLogin window= new DeliveryDriverLogin("DeliveryDriver Login");
			window.setLocationRelativeTo(null);
			window.setVisible(true);
			setVisible(false);//closes window
		}
	
		if (target== exitButton)
		{
			System.exit(0);
		}	
	
		}
}		

