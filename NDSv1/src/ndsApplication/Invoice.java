package ndsApplication;



public class Invoice {
	
	
	String name, address, iphone, pubName, areaCode, frequency, id;
	int bal, pubPrice;

	public Invoice(String ID, String name, String addr, String phoNum, String pName, String areaCode, String ifrequency, int balance, int pubPrice) {
		this.id = ID;
		this.name = name;
		this.address = addr;
		this.iphone = phoNum;
		this.areaCode = areaCode;
		this.pubName = pName;
		this.frequency = ifrequency;
		this.bal  = balance;
		this.pubPrice = pubPrice;
		
	}
	
	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public int getPubPrice() {
		return pubPrice;
	}



	public void setPubPrice(int pubPrice) {
		this.pubPrice = pubPrice;
	}



	public Invoice() {
		// TODO Auto-generated constructor stub
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIphone() {
		return iphone;
	}

	public void setIphone(String iphone) {
		this.iphone = iphone;
	}

	public String getPubName() {
		return pubName;
	}

	public void setPubName(String pubName) {
		this.pubName = pubName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequecny) {
		this.frequency = frequecny;
	}

	public int getBal() {
		return bal;
	}

	public void setBal(int bal) {
		this.bal = bal;
	}

}


