package ndsApplication;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.JButton;

public class DeliveryWindow {

	public JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;


	
	/**
	 * Create the application.
	 */
	public DeliveryWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Delivery ID");
		lblNewLabel.setBounds(137, 39, 70, 20);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(317, 39, 120, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblDeliveryDocketNumber = new JLabel("Delivery Docket Number");
		lblDeliveryDocketNumber.setBounds(137, 85, 120, 20);
		frame.getContentPane().add(lblDeliveryDocketNumber);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(317, 85, 120, 20);
		frame.getContentPane().add(textField_1);
		
		JLabel lblSubscriptionId = new JLabel("Subscription ID");
		lblSubscriptionId.setBounds(137, 129, 100, 20);
		frame.getContentPane().add(lblSubscriptionId);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(317, 129, 120, 20);
		frame.getContentPane().add(textField_2);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(135, 184, 102, 17);
		frame.getContentPane().add(lblAddress);
		
		JLabel lblPublication = new JLabel("Publication");
		lblPublication.setBounds(137, 254, 102, 17);
		frame.getContentPane().add(lblPublication);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(317, 251, 122, 20);
		frame.getContentPane().add(textField_4);
		
		JTextArea textArea = new JTextArea();
		textArea.setRows(3);
		textArea.setBounds(317, 180, 120, 60);
		frame.getContentPane().add(textArea);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Yes");
		rdbtnNewRadioButton.setBounds(316, 281, 50, 23);
		frame.getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("No");
		rdbtnNewRadioButton_1.setBounds(381, 281, 56, 23);
		frame.getContentPane().add(rdbtnNewRadioButton_1);
		
		JLabel lblNewLabel_1 = new JLabel("Delivered");
		lblNewLabel_1.setBounds(137, 285, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Comments");
		lblNewLabel_2.setBounds(137, 320, 56, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(317, 315, 120, 85);
		frame.getContentPane().add(textArea_1);
		
		JButton btnNewButton = new JButton("NEXT");
		btnNewButton.setBounds(485, 281, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnPrevious = new JButton("PREVIOUS");
		btnPrevious.setBounds(10, 281, 89, 23);
		frame.getContentPane().add(btnPrevious);
		
		JButton btnNewButton_1 = new JButton("SAVE");
		btnNewButton_1.setBounds(223, 411, 108, 39);
		frame.getContentPane().add(btnNewButton_1);
	}
}
