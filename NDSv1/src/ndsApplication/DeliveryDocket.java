package ndsApplication;

import java.util.Calendar;

public class DeliveryDocket {
	
	String name, address, subId, pName, aCode;
	
	public DeliveryDocket(String name, String addr, String subId, String pName, String areaCode) {
		this.name=name;
		this.address=addr;
		this.subId=subId;
		this.pName=pName;
		this.aCode=areaCode;
	}
	

	public String toString() {
					
			return "____________________________________________\n"
					+ "Customer Name : " + name+ "\nCustomer Address : " + address + "\nPublication Name : "+pName+ "\nArea Code : "+aCode+"\n\n"
					+ "____________________________________________";
			
		}
		
	public DeliveryDocket() {
		// TODO Auto-generated constructor stub
	}


	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getSubId() {
		return subId;
	}


	public void setSubId(String subId) {
		this.subId = subId;
	}


	public String getpName() {
		return pName;
	}

	public String getaCode() {
		return aCode;
	}

	public String checkDay() {
		String code = null;
		Calendar cal = Calendar.getInstance();
			//cal.set(yr,mon,day);
		
		if(cal.get(Calendar.DAY_OF_MONTH)==1)
			code = "monthly";
		if(cal.get(Calendar.DAY_OF_WEEK)==1)
			code ="weekly";
		if(cal.get(Calendar.DAY_OF_WEEK)>1&&cal.get(Calendar.DAY_OF_WEEK)<7)
			code = "daily";
		
		return code;
			
	}
}