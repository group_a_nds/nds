package ndsApplication;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

public class SendToFile {

	public void sendDeliveryDocket(ArrayList al) throws FileNotFoundException, UnsupportedEncodingException {
	PrintWriter writer = new PrintWriter("C:/NdsFiles/deliveryDocket.txt", "UTF-8");
	DeliveryDocket docket = new DeliveryDocket();
	Calendar cal = Calendar.getInstance();
    int month = cal.get(Calendar.MONTH);
    int day = cal.get(Calendar.DAY_OF_MONTH);
    int year = cal.get(Calendar.YEAR);
    String today =" " + day + "/" + (month + 1) + "/" + year+"\n";
	
	
	writer.println(" NDS Delivery Docket"+ today);
	writer.println();
	writer.println();
	for(int i=0;i<al.size();i++) {
		docket = (DeliveryDocket) al.get(i);
		writer.println(" Customer Name : "+docket.getName());
		writer.println(" Customer Address : "+docket.getAddress());
		writer.println(" Subscription ID : "+docket.getSubId());
		writer.println(" Publication Name : "+docket.getpName());
		writer.println(" Area Code : "+docket.getaCode());
		writer.println(" Delivered  YES []  NO []");
		writer.println(" Comments:");
		writer.println(" ________________________________________");
		writer.println(" ________________________________________");
		writer.println(" ________________________________________");
		writer.println(" ________________________________________");
		writer.println(" ________________________________________");

		writer.println();
		writer.println();

	}
	
	writer.close();
	}
	
}
