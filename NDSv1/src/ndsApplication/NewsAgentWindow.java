package ndsApplication;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.SystemColor;
import java.awt.Font;

public class NewsAgentWindow {

	public JFrame frame;
	private JTextField custName;
	private JTextField custAddr;
	private JTextField custPhone;
	private JTextField custEmail;
	private JTextField custArea;
	JTextArea textArea = new JTextArea();
	JComboBox<String> publicationCb = new JComboBox<String>();
	JComboBox<String> frequencyCb = new JComboBox<String>();
	
    Connection con;	
    Statement stmt;      
    ResultSet rs;	
    int count;
    int current;
	
	private String name, addr, phone, email, publication, frequency, area, id;
	private JTextField subID;
	private JTextField subFrequency;
	


	/**
	 * Create the application.
	 */
	public NewsAgentWindow() {
		initialize();
	}
	
	
	private void initialize() {
		
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.menu);
		frame.setBounds(100, 100, 900, 549);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Customer Name");
		lblNewLabel.setBounds(53, 36, 100, 20);
		frame.getContentPane().add(lblNewLabel);
		
		custName = new JTextField();
		custName.setBounds(163, 33, 127, 20);
		frame.getContentPane().add(custName);
		custName.setColumns(10);
		
		custAddr = new JTextField();
		custAddr.setColumns(10);
		custAddr.setBounds(163, 84, 127, 20);
		frame.getContentPane().add(custAddr);
		
		custPhone = new JTextField();
		custPhone.setColumns(10);
		custPhone.setBounds(163, 130, 127, 20);
		frame.getContentPane().add(custPhone);
		
		custEmail = new JTextField();
		custEmail.setColumns(10);
		custEmail.setBounds(163, 171, 127, 20);
		frame.getContentPane().add(custEmail);
		
		custArea = new JTextField();
		custArea.setColumns(10);
		custArea.setBounds(163, 215, 127, 20);
		frame.getContentPane().add(custArea);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(53, 79, 100, 20);
		frame.getContentPane().add(lblAddress);
		
		JLabel lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setBounds(53, 130, 100, 20);
		frame.getContentPane().add(lblPhoneNumber);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(53, 171, 100, 20);
		frame.getContentPane().add(lblEmail);
		
		JLabel lblAreaCode = new JLabel("Area Code");
		lblAreaCode.setBounds(53, 215, 100, 20);
		frame.getContentPane().add(lblAreaCode);
		
		JLabel lblPublication = new JLabel("Publication");
		lblPublication.setBounds(53, 256, 100, 20);
		frame.getContentPane().add(lblPublication);
		
		JLabel lblFrequency = new JLabel("Frequency");
		lblFrequency.setBounds(53, 299, 51, 20);
		frame.getContentPane().add(lblFrequency);
		
		JButton custClearBtn = new JButton("CLEAR");
		custClearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				custName.setText("");
				custAddr.setText("");
				custPhone.setText("");
				custEmail.setText("");
				custArea.setText("");
				
			}
		});
		custClearBtn.setBounds(53, 387, 89, 23);
		frame.getContentPane().add(custClearBtn);
		
		JButton custSaveBtn = new JButton("SAVE");
		custSaveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					if(!custName.getText().matches("[a-z A-Z]+")){
						JOptionPane.showMessageDialog(frame,
							    "Name must only be Alpha characters",
							    "ERROR", JOptionPane.ERROR_MESSAGE);

						throw new IllegalArgumentException();
						}
					else name=custName.getText();
					
					if(!custAddr.getText().matches("[a-z A-Z0-9]+")) {
						JOptionPane.showMessageDialog(frame,
							    "Address can only contain letters and numbers",
							    "ERROR", JOptionPane.ERROR_MESSAGE);

						throw new IllegalArgumentException();
						}
					else addr=custAddr.getText();
					
					
					if (!custPhone.getText().matches("[0-9]+")) {
						JOptionPane.showMessageDialog(frame,
							    "Phone number must be only numbers",
							    "ERROR", JOptionPane.ERROR_MESSAGE);

						throw new IllegalArgumentException();
						}
					if (custPhone.getText().length()>10 || custPhone.getText().length()<10) {
						JOptionPane.showMessageDialog(frame,
							    "Phone number must be 10 digits long",
							    "ERROR", JOptionPane.ERROR_MESSAGE);

						throw new IllegalArgumentException();
						}
					else phone=custPhone.getText();
					
					if(!custEmail.getText().contains("@")) {
						JOptionPane.showMessageDialog(frame,
							    "Email must contain the @ symbol",
							    "ERROR", JOptionPane.ERROR_MESSAGE);

						throw new IllegalArgumentException();
						}
					else email=custEmail.getText();
					
					
					if (!custArea.getText().matches("[0-9]+")) {
						JOptionPane.showMessageDialog(frame,
							    "Area code must be a number",
							    "ERROR", JOptionPane.ERROR_MESSAGE);

						throw new IllegalArgumentException();
						}
					else area=(custArea.getText());
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(frame,
						    "Please ensure fields are valid",
						    "ERROR", JOptionPane.ERROR_MESSAGE);
					custName.setText("");
					custAddr.setText("");
					custPhone.setText("");
					custEmail.setText("");
					custArea.setText("");
					
				}
				
				Customer customer = new Customer(name, addr, phone, email, area);

				publication=(String) publicationCb.getSelectedItem();
				frequency=(String) frequencyCb.getSelectedItem();
				
				if (validateNoBlankFields()) {
					DatabaseHandler dbHandler = new DatabaseHandler();
					dbHandler.saveCustomerDetails(customer);
					try {
						dbHandler.saveSubscriptions(publication,frequency,name);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//send to database handler
				}
				
				
				

				
			}
		});
		custSaveBtn.setBounds(201, 387, 89, 23);
		frame.getContentPane().add(custSaveBtn);
		
		JButton btnNewButton = new JButton("SEARCH");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				name = custName.getText();
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
					Customer retnd = dbHandler.searchCustByName(name);
					
					custName.setText(retnd.getName());
					custAddr.setText(retnd.getAddress());
					custPhone.setText("0"+retnd.getPhone());
					custEmail.setText(retnd.getEmail());
					custArea.setText(retnd.getAreaCode());
					
					frequencyCb.setSelectedItem(DatabaseHandler.getFreq());
					publicationCb.setSelectedItem(DatabaseHandler.getPubName());
					
					if(retnd.getName() == name ) {
						//default title and icon
						JOptionPane.showMessageDialog(frame,
								   retnd.getName() +  " found in Database.");					
					}				
				} catch (SQLException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(398, 36, 110, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnUpdate = new JButton("DELIVERY DOCKET");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Calendar cal = Calendar.getInstance();
	            int month = cal.get(Calendar.MONTH);
	            int day = cal.get(Calendar.DAY_OF_MONTH);
	            int year = cal.get(Calendar.YEAR);
	            String today =" NDS Delivery Docket "+'\t' + day + "/" + (month + 1) + "/" + year+"\n";
				DeliveryDocket getCode = new DeliveryDocket();
				ArrayList al= new ArrayList<>();
				String code = getCode.checkDay();
				SendToFile docketOut=new SendToFile();
				
				
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
					al=dbHandler.createDeliveryDocket(code);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				textArea.append(today);
				
				for(int i=0;i<al.size();i++) {
				textArea.append(al.get(i).toString());
				}
				
				try {
					docketOut.sendDeliveryDocket(al);
				} catch (FileNotFoundException | UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		btnUpdate.setBounds(733, 22, 141, 34);
		frame.getContentPane().add(btnUpdate);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = custName.getText();
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
						
						//default icon, custom title
						int n = JOptionPane.showConfirmDialog(
						    frame,
						    "Are you sure you would like to delete " + name + "?" ,
						    "Confirm",
						    JOptionPane.YES_NO_OPTION);
						
						if(n == JOptionPane.YES_OPTION) {
							dbHandler.archiveCustomer(name);
							custName.setText(null);
							custAddr.setText(null);
							custArea.setText(null);
							custEmail.setText(null);
							custPhone.setText(null);

							
							//default title and icon
							JOptionPane.showMessageDialog(frame,
							   name +  " Deleted from Database.");
							custName.setText(null);		
						
						}
					
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(398, 112, 110, 23);
		frame.getContentPane().add(btnDelete);
		
		JButton button = new JButton("UPDATE");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				name=custName.getText();
				publication=(String) publicationCb.getSelectedItem();
				frequency=(String) frequencyCb.getSelectedItem();
				
				
				
				DatabaseHandler dbHandler = new DatabaseHandler();
				
				try {
					dbHandler.updateSubDetails(name, publication, frequency);
				} catch (SQLException e) {
					
				}
				
				
				
				
			}
		});
		button.setBounds(398, 188, 110, 23);
		frame.getContentPane().add(button);
		
		JButton btnInvoice = new JButton("INVOICE");
		btnInvoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				area = custArea.getText();
				Calendar cal = Calendar.getInstance();
	            int month = cal.get(Calendar.MONTH);
	            int day = cal.get(Calendar.DAY_OF_MONTH);
	            int year = cal.get(Calendar.YEAR);
	            String header =" NDS Invoice for Area " +area+"\t\t" + day + "/" + (month + 1) + "/" + year+"\n";
	            Invoice invoice = new Invoice();
			
				ArrayList<Invoice> bill= new ArrayList<>();
				
				//String code = getCode.checkDay();
				//SendToFile docketOut=new SendToFile();
				
				
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
					bill=dbHandler.showInvoice(area);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				textArea.append(header);
				
				for(int i=0;i<bill.size();i++) {
					invoice = (Invoice) bill.get(i);
					textArea.append(" _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ");
					textArea.append( "\n\n Customer: " +invoice.getId() + "\t\t\t" +day + "/" + (month + 1) + "/" + year+"\n");
					textArea.append("\n "+ "Bill To: ");
					textArea.append("\n\n "+invoice.getName());
					textArea.append("\n "+invoice.getAddress());
					textArea.append("\n " + "0"+invoice.getIphone());
					textArea.append("\n_____________________________________________");
					textArea.append("\n\n " + invoice.getPubName() + "\t\t €"+ invoice.getPubPrice());
					
					textArea.append("\n\n\n\n____________________________________________");
					textArea.append("\n Frequency: " + "\t\t\t"  + invoice.getFrequency());
					textArea.append("\n_____________________________________________");
					textArea.append("\n Balance: " + "\t\t\t€"  + invoice.getBal());
					textArea.append("\n_____________________________________________");
					textArea.append("\n\n\n _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ");
				}
			}
			
		});
		btnInvoice.setBounds(548, 22, 141, 34);
		frame.getContentPane().add(btnInvoice);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(548, 11, -40, 439);
		frame.getContentPane().add(separator);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(548, 63, 326, 387);
		frame.getContentPane().add(scrollPane);
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 11));
		scrollPane.setViewportView(textArea);
		
		
		publicationCb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		publicationCb.setModel(new DefaultComboBoxModel<String>(new String[] {"Irish Daily Mail", "The Daily Telegraph", "Sunday World", "Irish Garden", "Golf Digest Ireland"}));
		publicationCb.setBounds(163, 256, 127, 20);
		frame.getContentPane().add(publicationCb);
		
		
		frequencyCb.setModel(new DefaultComboBoxModel<String>(new String[] {"Daily", "Weekly", "Monthly"}));
		frequencyCb.setBounds(163, 299, 127, 20);
		frame.getContentPane().add(frequencyCb);
		
		JButton staffBtn = new JButton("STAFF");
		staffBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				StaffWindow.main(null);
			}
		});
		staffBtn.setBounds(398, 340, 110, 23);
		frame.getContentPane().add(staffBtn);
		
		JButton pubBtn = new JButton("PUBLICATIONS");
		pubBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PublicationsWindow.main(null);
				
			}
		});
		pubBtn.setBounds(398, 264, 110, 23);
		frame.getContentPane().add(pubBtn);
		
		JButton btnPrint = new JButton("PRINT");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					textArea.print();
				} catch (PrinterException e) {
					
					JOptionPane.showMessageDialog(frame,
							"Error with print. Please try again!",
						    "ERROR", JOptionPane.ERROR_MESSAGE);
					
				}
				
			}
		});
		btnPrint.setBounds(733, 461, 141, 34);
		frame.getContentPane().add(btnPrint);
		
/*		JLabel lblSubid = new JLabel("SubID");
		lblSubid.setBounds(53, 351, 46, 14);
		frame.getContentPane().add(lblSubid);
		
		subID = new JTextField();
		subID.setBounds(168, 348, 86, 20);
		frame.getContentPane().add(subID);
		subID.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("SubFreq");
		lblNewLabel_1.setBounds(53, 394, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		subFrequency = new JTextField();
		subFrequency.setBounds(168, 391, 86, 20);
		frame.getContentPane().add(subFrequency);
		subFrequency.setColumns(10);
		
		JButton btnSearchBySub = new JButton("Search By Sub");
		btnSearchBySub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				id = subID.getText();			
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
					Subscriptions retnd = dbHandler.searchSubByID(id);
						subID.setText(retnd.getSubID());
						subFrequency.setText(retnd.getSubFreq());
					if(retnd.getSubID() == null ) {
						
						JOptionPane.showMessageDialog(frame,
						id +  " subscription found in Database.");
								
						}
					else
							JOptionPane.showMessageDialog(frame,
								retnd.getSubID() +  " subscription not found in Database.");			
						}					
					
				 catch (SQLException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnSearchBySub.setBounds(399, 323, 109, 23);
		frame.getContentPane().add(btnSearchBySub);*/
		
		frequencyCb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
	}
	
	public boolean validateNoBlankFields() {
		
				if (! validateField( custName, "Please enter a name"))
				    return false;
				  else
				if (! validateField( custAddr, "Please enter an address"))
				    return false;
				  else
				if (! validateField( custEmail, "Please enter an email address"))
				    return false;
				  else
				if (! validateInteger( custArea, "Please enter an area code"))
					return false;
				  else
				if (! validateInteger( custPhone, "Please enter a phone number"))
					return false;
				  else
				    return true;
				}

				// test if field is empty
				public boolean validateField( JTextField f, String errormsg ) {
				  if ( f.getText().equals("") )
				    return failedMessage( f, errormsg );
				  else
				    return true; // validation successful
				}

				public boolean validateInteger( JTextField f, String errormsg ) {
				  try
				  {  // try to convert input to integer
				    int i = Integer.parseInt(f.getText());

				    // input must be greater than 0
				    // if it is, success
				    if ( i > 0 )
				      return true; // success, validation succeeded
				   }
				   catch(Exception e)
				   {
				      // if conversion failed, or input was <= 0,
				      // fall-through and do final return below
				   }
				   return failedMessage( f, errormsg );
				}

				public boolean failedMessage(JTextField f, String errormsg) {
				  JOptionPane.showMessageDialog(null, errormsg); // give user feedback
				  f.requestFocus(); // set focus on field, so user can change
				  return false; // return false, as validation has failed
				}
}
	
	

