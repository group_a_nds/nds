package ndsApplication;

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ExceptionHandlers.PubPriceExceptionHandler;
import ExceptionHandlers.StaffNameExceptionHandler;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class PublicationsWindow extends JFrame{

	private JFrame frame;
	private JTextField PubNameTF;
	private JTextField PubPriceTF;
    private String pubName;
	public double pubPrice;
	
	private String name;
	
	Connection con;	
    Statement stmt;      
    ResultSet rs;	
    int count;
    int current;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PublicationsWindow window = new PublicationsWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public boolean validateZeroNum(double pubPrice) throws PubPriceExceptionHandler {
		
		boolean result = false;
		
		if(pubPrice <= 0) {
			JOptionPane.showMessageDialog(frame,
				    "Price must be greater than 0",
				    "ERROR", JOptionPane.ERROR_MESSAGE);
			
			throw new PubPriceExceptionHandler("Price must be greater than 0");	
			
			}
			
		return result;
		
	}

	
	

	/**
	 * Create the application.
	 */
	public PublicationsWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Publications");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPubName = new JLabel("Publication Name");
		lblPubName.setBounds(37, 64, 110, 14);
		frame.getContentPane().add(lblPubName);
		
		PubNameTF = new JTextField();
		PubNameTF.setBounds(238, 61, 158, 20);
		frame.getContentPane().add(PubNameTF);
		PubNameTF.setColumns(10);
		
		JLabel lblPubPrice = new JLabel("Publication Price");
		lblPubPrice.setBounds(37, 112, 99, 14);
		frame.getContentPane().add(lblPubPrice);
		
		PubPriceTF = new JTextField();
		PubPriceTF.setBounds(238, 109, 158, 20);
		frame.getContentPane().add(PubPriceTF);
		PubPriceTF.setColumns(10);
//Save Button			
		JButton saveButton = new JButton("SAVE");
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!PubNameTF.getText().matches("[a-z A-Z 0-9]+" ) ){
					JOptionPane.showMessageDialog(frame,
						    "Publication Name must only be Alphanumeric Characters",
						    "ERROR", JOptionPane.ERROR_MESSAGE);}
				else {
					if (PubNameTF.getText().length()< 3)
					{
						JOptionPane.showMessageDialog(frame,
							    "Publication Name must be greater than 3 Character",
							    "ERROR", JOptionPane.ERROR_MESSAGE);
						}
					else
						pubName = PubNameTF.getText();
				
				if (PubPriceTF.getText().length()<=0)
					{
					JOptionPane.showMessageDialog(frame,
						    "Publication price must be positive number",
						    "ERROR", JOptionPane.ERROR_MESSAGE);
					}
				else 
					pubPrice = Double.parseDouble(PubPriceTF.getText());PubPriceTF.getText();
				
					Publications publications = new Publications(pubName, pubPrice);

					DatabaseHandler dbHandler = new DatabaseHandler();
					dbHandler.savePublicationDetails(publications);
				}								
			}
		});
		saveButton.setBounds(71, 210, 110, 23);
		frame.getContentPane().add(saveButton);
//Search Button		
		JButton searchButton = new JButton("Search");
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
								
				name = PubNameTF.getText();			
				DatabaseHandler dbHandler = new DatabaseHandler();
				try {
					Publications retnd = dbHandler.searchPubByName(name);
					
					
					PubPriceTF.setText("" +retnd.getPubPrice());
					
					PubNameTF.setText(retnd.getPubName());
							
					if(retnd.getPubName() == null ) {
						
						JOptionPane.showMessageDialog(frame,
						name +  " publication not found in Database.");
								
						}}
					
				 catch (SQLException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				
			}
		});
		searchButton.setBounds(252, 176, 110, 23);
		frame.getContentPane().add(searchButton);
		
		JButton btnUpdatePrice = new JButton("Update Price");
		btnUpdatePrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				pubName = PubNameTF.getText();
				pubPrice = Double.parseDouble(PubPriceTF.getText());PubPriceTF.getText();
				
				try {
					validateZeroNum(pubPrice);
		
					
					
					DatabaseHandler dbHandler = new DatabaseHandler();
					dbHandler.updatePubDetails(pubName, pubPrice);
			
				} catch (PubPriceExceptionHandler e1) {
					
					
					PubPriceTF.setText(null);
					PubPriceTF.requestFocus();
				}
				
			}

		});

		btnUpdatePrice.setBounds(252, 210, 110, 23);
		frame.getContentPane().add(btnUpdatePrice);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				name = PubNameTF.getText();
				
				DatabaseHandler dbHandler = new DatabaseHandler();
				
				Publications retnd = null;
			
				try {
					retnd = dbHandler.searchPubByName(name);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				PubNameTF.setText(retnd.getPubName());
				
				if(retnd.getPubName() == null ) {
					//default title and icon
					JOptionPane.showMessageDialog(frame,
					   name +  " not found in Database.");
					
				}else {
				
				//default icon, custom title
				int n = JOptionPane.showConfirmDialog(
				    frame,
				    "Are you sure you would like to delete " + name + "?" ,
				    "Confirm",
				    JOptionPane.YES_NO_OPTION);
				
				PubNameTF.setText(null);
				
				if(n == JOptionPane.YES_OPTION) {
			
					dbHandler.DelPubByName(name);
					
					//default title and icon
					JOptionPane.showMessageDialog(frame,
					   name +  " Deleted from Database.");
					PubNameTF.setText(null);
				
				
				}}
				
				
				
			}
		});
		btnDelete.setBounds(71, 176, 110, 23);
		frame.getContentPane().add(btnDelete);
	}
	
	

}
