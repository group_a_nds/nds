package ndsApplication;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.*;


import java.util.*; 

class DeliveryDriverLogin extends JFrame implements ActionListener{
	
	private final JLabel LogoLabel = new JLabel("");
	private final JLabel UsernameLabel = new JLabel("Username");
	private final JTextField UsernameTF = new JTextField();
	private final JLabel PasswordLabel = new JLabel("Password");
	private final JPasswordField PasswordTF = new JPasswordField();
	private final JButton loginButton = new JButton("Login");
	private final JButton resetButton = new JButton("Reset");
	private final JButton exitButton = new JButton("Exit");

//CONSTRUCTOR
	public DeliveryDriverLogin(String s){
		super(s);
		
		getContentPane().setBackground(SystemColor.menu);

		Container content=getContentPane();
		getContentPane().setLayout(null);
		
//Logo		
		LogoLabel.setBounds(47, 11, 215, 225);
		getContentPane().add(LogoLabel);
		Image img = new ImageIcon(this.getClass().getResource("/delivery.png")).getImage();
		LogoLabel.setIcon(new ImageIcon(img));
		UsernameLabel.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		UsernameLabel.setBounds(10, 268, 72, 14);
		
		getContentPane().add(UsernameLabel);
		UsernameTF.setColumns(10);
		UsernameTF.setBounds(107, 266, 156, 20);
		
		getContentPane().add(UsernameTF);
		PasswordLabel.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		PasswordLabel.setBounds(10, 299, 87, 14);
		
		getContentPane().add(PasswordLabel);
		PasswordTF.setBounds(107, 297, 156, 20);
		
		getContentPane().add(PasswordTF);
		loginButton.setBounds(10, 328, 77, 23);
		
		getContentPane().add(loginButton);
		resetButton.setBounds(97, 328, 77, 23);
		
		getContentPane().add(resetButton);
		exitButton.setBounds(184, 328, 77, 23);
		
		getContentPane().add(exitButton);
		
		
		loginButton.addActionListener(this);		
		resetButton.addActionListener(this);
		exitButton.addActionListener(this);
		setSize(288,400);    setVisible(true);
		}

// Buttons Action Performed
	public void actionPerformed(ActionEvent e){
				Object target=e.getSource();
				
				if (target==loginButton)
				{
					String userName = UsernameTF.getText();
					String password = PasswordTF.getText();
					
					if (userName.length() >= 1 && userName.length() <=5) {
						JOptionPane.showMessageDialog(null,"Username length less than 5","NDS",JOptionPane.ERROR_MESSAGE);
						UsernameTF.setText(null);
						PasswordTF.setText(null);
					}
					
					else if (password.length() >= 1 && password.length() <=3) {
						JOptionPane.showMessageDialog(null,"Password length less than 4 characters","NDS",JOptionPane.ERROR_MESSAGE);
						UsernameTF.setText(null);
						PasswordTF.setText(null);
					}
					
					
					else if (userName.length() >= 16 && userName.length() <= 100){
						JOptionPane.showMessageDialog(null,"Greater than 15","NDS",JOptionPane.ERROR_MESSAGE);
						UsernameTF.setText(null);
						PasswordTF.setText(null);
					}
					
					else if  (password.length() >= 10 && password.length() <= 100) {
						JOptionPane.showMessageDialog(null,"Password greater than 9 characters","NDS",JOptionPane.ERROR_MESSAGE);
						UsernameTF.setText(null);
						PasswordTF.setText(null);
					}
					
					else if ((userName.length() >= 6  && userName.length() <=15) && (password.length() >= 4  && password.length() <=9) && (userName.contains("zalman") && password.contains("zalman"))){
						DeliveryWindow window = new DeliveryWindow();
					   
						window.frame.setVisible(true);
						//window.setLocationRelativeTo(null);
						setVisible(false);//closes window
					}
					
					else
					{
						JOptionPane.showMessageDialog(null,"Wrong Login/Password","NDS",JOptionPane.ERROR_MESSAGE);
						
					}
				}
				
				if (target==resetButton)
				{
					UsernameTF.setText(null);
					PasswordTF.setText(null);
				}
				
				if (target==exitButton)
				{
					int selectedOption =JOptionPane.showConfirmDialog(null,"Do you want to close programm?",
				            "NDS",JOptionPane.YES_NO_OPTION);
				            if (selectedOption == JOptionPane.YES_OPTION){
				            	System.exit(0);
				            }
				}
				
	
		}
}		

