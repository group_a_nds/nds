package ndsApplication;
/**
 * @author Vlad
 *
 */
//Publications
public class Publications {

 private String pubName;
 private double pubPrice;
 
/**
* Publications Class
*/
	
	
	public Publications( String pubName,double pubPrice ) 
	{
		this.pubName=pubName;
		this.pubPrice=pubPrice;
	}
	public Publications() {
		// TODO Auto-generated constructor stub
	}
	public String getPubName() 
	{
		return pubName;
	}

	public void setPubName(String pubName) 
	{
		this.pubName = pubName;
	}

	public double getPubPrice() 
	{
		return pubPrice;
	}
	
	public void setPubPrice(double pubPrice)
	{
		this.pubPrice = pubPrice;
	}
	
	
	
}
