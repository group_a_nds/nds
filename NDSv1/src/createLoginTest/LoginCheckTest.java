package createLoginTest;

import ExceptionHandlers.LoginCheckExceptionHandler;
import junit.framework.TestCase;

public class LoginCheckTest extends TestCase  {
	    // Test No. 1
		// Objective: To test UN in range 1 to 5 chars
		// Input(s): UserName = "abc"
		// Expected Output: Exception Msg: "Username length less than 6 characters"
		
		public void testvalidateUserName001() {
			
			LoginCheck testObject = new LoginCheck();
			
			try {
			
				boolean result = testObject.validateUserName("abc");
				fail("Exception expected");
				
			}
			catch (LoginCheckExceptionHandler e){
				
				assertEquals("Username length less than 6 characters", e.getMessage());
			}
		}

		// Test No. 2
		// Objective: To test UN in range 6 to 15 chars
		// Input(s): UserName = "abcdefgh"
		// Expected Output: True

		public void testvalidateUserName002() {
			
			LoginCheck testObject = new LoginCheck();
			
			try {
				
				assertEquals(true, testObject.validateUserName("abcdefgh"));
				
			}
			catch (LoginCheckExceptionHandler e) {
				
				fail("Exception NOT expected");
				
			}	
		}

		// Test No. 3
		// Objective: To test UN in range 16 to MAxINT chars
		// Input(s): UserName = "abcdefghiljklmnopqrst"
		// Expected Output: False

		
		public void testvalidateUserName003() {
			
			LoginCheck testObject = new LoginCheck();
			
			try {
			
				boolean result = testObject.validateUserName("abcdefghiljklmnopqrst");
				fail("Exception expected");
				
			}
			catch (LoginCheckExceptionHandler e){
				
				assertEquals("Username greater than 15 characters", e.getMessage());
			}
			
			
		}
		// Test No. 4
				// Objective: To test Password in range 1 to 3 chars
				// Input(s): UserName = "abc"
				// Expected Output: Exception Msg: "Password length less than 4 characters"
				
				public void testvalidatePassword001() {
					
					LoginCheck testObject = new LoginCheck();
					
					try {
					
						boolean result = testObject.validatePassword("abc");
						fail("Exception expected");
						
					}
					catch (LoginCheckExceptionHandler e){
						
						assertEquals("Password length less than 4 characters", e.getMessage());
					}
				}
			// Test No. 5
			// Objective: To test Password in range 4 to 9 chars
			// Input(s): UserName = "abcdefgh"
			// Expected Output: True

			public void testvalidatePassword002() {
				
				LoginCheck testObject = new LoginCheck();
				
				try {
					
					assertEquals(true, testObject.validateUserName("abcd123"));
					
				}
				catch (LoginCheckExceptionHandler e) {
					
					fail("Exception NOT expected");
					
				}	
			}
			// Test No. 6
			// Objective: To test Password in range 10 to MAxINT chars
			// Input(s): UserName = "abcdefghiljklmn543544"
			// Expected Output: False

			
			public void testvalidatePassword003() {
				
				LoginCheck testObject = new LoginCheck();
				
				try {
				
					boolean result = testObject.validatePassword("abcdefghiljklmnopqrst");
					fail("Exception expected");
					
				}
				catch (LoginCheckExceptionHandler e){
					
					assertEquals("Password greater than 9 characters", e.getMessage());
				}
				
				
			}
		

	}

