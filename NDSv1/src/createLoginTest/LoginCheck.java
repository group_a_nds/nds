package createLoginTest;

import ExceptionHandlers.LoginCheckExceptionHandler;

public class LoginCheck {
	
	boolean validateUserName(String userName) throws LoginCheckExceptionHandler {
		
		// validateUserName returns true if the userName is between 
				// 6 and 15 characters. Otherwise it returns false.
				boolean result = false;
				
				
				if (userName.length() >= 1 && userName.length() <=5)
					throw new LoginCheckExceptionHandler("Username length less than 6 characters");
				else if (userName.length() >= 6  && userName.length() <=15)
					result = true;
				else if (userName.length() >= 16 && userName.length() <= 100)
					throw new LoginCheckExceptionHandler("Username greater than 15 characters");
				
				return result;
		
	}
	
	boolean validatePassword(String password) throws LoginCheckExceptionHandler{
				// validatePassword returns true if the password is between 
				// 4 and 9 characters. Otherwise it returns false.
				boolean result = false;
				
				
				if (password.length() >= 1 && password.length() <=3)
					throw new LoginCheckExceptionHandler("Password length less than 4 characters");
				else if (password.length() >= 4  && password.length() <=9)
					result = true;
				else if (password.length() >= 10 && password.length() <= 100)
					throw new LoginCheckExceptionHandler("Password greater than 9 characters");
				
				return result;
	}
}
