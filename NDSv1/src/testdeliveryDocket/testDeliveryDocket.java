package testdeliveryDocket;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ndsApplication.DeliveryDocket;

class testDeliveryDocket {

	/*
	 * These tests were run with the method signature
	 *  public String checkDay(int yr, int mon, int day)
	 *  
	 * i have now removed the arguments in the method as these tests have passed
	 * the method will now only perform checks on the current date
	 * which will be generated each time the method is called
	 * 
	 * if these tests are to be run again
	 * please insert integers in the format yyyy,mm,dd
	 * 
	 */
	@Test
	void testDaily() {
		
		DeliveryDocket test = new DeliveryDocket();
		
		String result = test.checkDay();
		
		assertEquals("daily", result);			
		
	}
	
	@Test
	void testWeekly() {
		
		DeliveryDocket test = new DeliveryDocket();
		
		String result = test.checkDay();
		
		assertEquals("weekly", result);			
		
	}
	
	@Test
	void testMonthly() {
		
		DeliveryDocket test = new DeliveryDocket();
		
		String result = test.checkDay();
		
		assertEquals("monthly", result);			
		
	}

}
