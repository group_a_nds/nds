package createPubTests;
//Test Publications
public class Test_Pub
{
	

	public boolean testPubName(String input){
		
	String [] pub =new String []{"Irish Daily Mail","The Daily Telegraph","Sunday World","Irish Garden","Golf Digest Ireland"};
	for (int i=0;i<5;i++){
		if(pub[i].matches(input)){
			return true;
		}
	}
	return false;
	}
	
	public boolean testPubPrice(String input){
		
	String [] pub = new String []{"1.40","2","2.50","4.80","6.60"};
	for (int i=0;i<5;i++){
		if(pub[i].matches(input)){
			return true;
		}
	}
	return false;
	}
	
	public boolean testAlphaNumChar(String name) {
		return name.matches("[a-z A-Z 0-9]+");		
	}
	
	public boolean testNonAlphaNumChar(String name){
		if (name.matches("[a-z A-Z 0-9]+")) {
			return true;
		}
		return false;
	}
	
	public boolean testNumPositive(double positive) {
		if(positive>0) {
			return true;
			}
		return false;
	}
	
	public boolean testPubNameLength(String name) {
		if(name.length()<3) {
			return true;
			}
		return false;
	}
}
	

