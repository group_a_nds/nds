package createPubTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import createCustTests.TestCustomer;
//Test for Pub
class TestPubPricePositiveJunit {
	@Test
	
	//Test 					    No.7
		//Objective:  			To ensure Publication price greater that 0
		//Input(s)    			pubPrice = - 2.60
		//Expected Output(s)	False
		//
	void test() {
		Test_Pub test = new Test_Pub();
		boolean output = test.testNumPositive(-2.60);
		assertEquals(false, output);
	}

}
