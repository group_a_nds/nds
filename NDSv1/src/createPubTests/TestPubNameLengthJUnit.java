package createPubTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
//Test for Pub
class TestPubNameLengthJUnit {

	@Test
	//Test 					No.4
	//Objective:  			Verify that Publication name accepts names greater than 3 characters only
	//Input(s)    			pubName = "ABC"
	//Expected Output(s)	False
	//
		void test() {
			Test_Pub test = new Test_Pub();
			boolean output = test.testPubNameLength("ABC");
			assertEquals(false, output);
	}

}
