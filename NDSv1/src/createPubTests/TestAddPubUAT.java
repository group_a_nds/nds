package createPubTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ndsApplication.Publications;
import ndsApplication.Staff;
//Test for Pub
class TestAddPubUAT {

	// Test No. 1
	// Objective: Test Value passed to Constructor
	// Input(s): Name = "Sunday World",  Price = "2.50"
	// Expected Output: Same Publication Name and Price retrieved.

@Test
void test_PubObjTrue() {
	Publications publications = new Publications("Sunday World", 2);
	
	assertEquals("Sunday World", publications.getPubName());
	assertEquals("2.50", publications.getPubPrice());
}
}
