package createPubTests;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
//Test for Pub
public class TestPubNameJUnit {

	@Test
	//Test 					No.3
	//Objective:  			To ensure selected publication name value matches value in the database
	//Input(s)    			pubName = "The Daily Telegraph"
	//Expected Output(s)	True
	//
	public void test() {
		Test_Pub test = new Test_Pub();
		boolean output = test.testPubName("The Daily Telegraph");
		assertEquals(true, output);
	}
}
