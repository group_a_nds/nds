package createPubTests;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
//Test for Pub

public class TestPubPriceJUnit {

	@Test
	//Test 					No.6
	//Objective:  			To ensure selected publication price value matches value in the database
	//Input(s)    			pubPrice = "1.40"
	//Expected Output(s)	True
	//
	public void test() {
		Test_Pub test = new Test_Pub();
		boolean output = test.testPubPrice("1.40");
		assertEquals(true, output);
	}

}
