package createPubTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ExceptionHandlers.PubPriceExceptionHandler;
import ExceptionHandlers.StaffNameExceptionHandler;
import createCustTests.TestCustomer;
import ndsApplication.PublicationsWindow;
import ndsApplication.StaffWindow;

class NotZero {
	

	@Test
	// Objective: To test if Num is not zero
	// Input(s):  = 0
	// Expected Output: Exception Msg: "Price must be greater than 0"
	
	public void testvalidatePrice001()  {
		
		PublicationsWindow test = new PublicationsWindow();
		
		try {
		
			boolean result = test.validateZeroNum(0);
			fail("Exception expected");
			
		}
		catch (PubPriceExceptionHandler e){
			
			assertEquals("Price must be greater than 0", e.getMessage());
		}
		
	}	
	
	

}
