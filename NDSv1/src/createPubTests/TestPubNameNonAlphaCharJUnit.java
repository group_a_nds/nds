package createPubTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import createCustTests.TestCustomer;
//Test for Pub
class TestPubNameNonAlphaCharJUnit {

	@Test
	//Test 					No.5
	//Objective:  			Verify that Publication name accepts only alphanumeric input 
	//Input(s)    			pubName = "&&&&&&"
	//Expected Output(s)	False
	//
	void test() {
	
			Test_Pub test = new Test_Pub();
			boolean output = test.testNonAlphaNumChar("&&&&&&");
			assertEquals(false, output);
		}
	}


