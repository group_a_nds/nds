package createPubTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import createCustTests.TestCustomer;
//Test for Pub
class TestPubNameAlphaCharJUnit {

	@Test
	//Test 					No.2
	//Objective:  			Verify that Publication name accepts only alphanumeric input 
	//Input(s)    			pubName = "Sunday World"
	//Expected Output(s)	True
	//
	void test() {
		Test_Pub test = new Test_Pub();
		boolean output = test.testAlphaNumChar("Sunday World");
		assertEquals(true, output);
	}

}
