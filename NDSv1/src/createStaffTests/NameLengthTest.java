/**
 * 
 */
package createStaffTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ndsApplication.StaffWindow;
import ExceptionHandlers.StaffNameExceptionHandler;

/**
 * @author liamh
 *
 */
class NameLengthTest  {
	
	@Test
	// Test No. 1
	// Objective: To test UN in range 1 to 5 chars
	// Input(s): UserName = "abc"
	// Expected Output: Exception Msg: "Length less than 5"
	
	public void testvalidateName001() {
		
		StaffWindow testObject = new StaffWindow();
		
		try {
		
			boolean result = testObject.validateNameLength("ab");
			fail("Exception expected");
			
		}
		catch (StaffNameExceptionHandler e){
			
			assertEquals("Length less than 3", e.getMessage());
		}
		
		
		
	}
	
	@Test
	// Test No. 2
	// Objective: To test UN in range 6 to 24 chars
	// Input(s): UserName = "abcdefgh"
	// Expected Output: True

	public void testvalidateUserName002() {
		
		StaffWindow testObject = new StaffWindow();
		
		try {
			
			assertEquals(true, testObject.validateNameLength("abcdefgh"));
			
		}
		catch (StaffNameExceptionHandler e) {
			
			fail("Exception NOT expected");
			
		}
		
		
	}
	
	@Test
	// Test No. 3
	// Objective: To test UN in range 16 to MAxINT chars
	// Input(s): UserName = "abcdefghiljklmnopqrst"
	// Expected Output: False

	
	public void testvalidateUserName003() {
		
		StaffWindow testObject = new StaffWindow();
		
		try {
		
			boolean result = testObject.validateNameLength("abcdefghiljklmnopqrstuvwxyz");
			fail("Exception expected");
			
		}
		catch (StaffNameExceptionHandler e){
			
			assertEquals("Length greater than 25", e.getMessage());
		}
		
		
	}
	

}
	

