package createStaffTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ndsApplication.Staff;


class StaffTest {
	
	// Test No. 1
		// Objective: Test Value passed to Constructor
		// Input(s): Name = "Liam Hickey"
		// Expected Output: Equals name retrieved.

	@Test
	void test_StaffObjTrue() {
		Staff staff = new Staff("Liam Hickey");
		
		assertEquals("Liam Hickey", staff.getName());
	}
	
	
	

}
