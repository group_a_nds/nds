package createStaffTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ndsApplication.StaffWindow;
import ExceptionHandlers.StaffNameExceptionHandler;



class StaffWindowTestForAlphabetCharacters {
	
	@Test
		// Test No. 1
		// Objective: To test numeric characters are not accepted
		// Input(s): Name = "John M9prphy"
		// Expected Output: Exception Msg: "Must be Alphabetic Characters"
		
	public void TestForAlpha001(){
	
	StaffWindow testAlpha = new StaffWindow();
	
	try {
	
		boolean output = !testAlpha.validateAplhaChar("John M9rphy");
		fail("Exception expected");
		
	
		
	}
	catch (StaffNameExceptionHandler e){
		
		assertEquals("Must be Alphabetic Characters", e.getMessage());
	}
	}
	
	@Test
		// Test No. 2
		// Objective: To test Alphabetic characters are accepted
		// Input(s): Name = "John Murphy"
		// Expected Output: True
	public void TestForAlpha002(){
	
	StaffWindow testAlpha = new StaffWindow();
	
	try {
	
		assertEquals(true, !testAlpha.validateAplhaChar("John Murphy"));
		
	}
	catch (StaffNameExceptionHandler e){
		
		fail("Exception NOT expected");
	}
	}
	

	
	

}
