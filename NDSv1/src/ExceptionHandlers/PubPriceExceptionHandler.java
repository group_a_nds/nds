package ExceptionHandlers;

public class PubPriceExceptionHandler extends Exception {
	
	private static final long serialVersionUID = 1L;
	String message;
	
	public PubPriceExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
		
	}	


}
