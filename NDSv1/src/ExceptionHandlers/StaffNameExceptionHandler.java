package ExceptionHandlers;

import ndsApplication.StaffWindow;

public class StaffNameExceptionHandler extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public StaffNameExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
		
	}	

}
