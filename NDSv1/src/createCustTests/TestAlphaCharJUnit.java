package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestAlphaCharJUnit {

	@Test
	void test() {
		TestCustomer test = new TestCustomer();
		boolean output = test.testAlphaChar("John Murphy");
		assertEquals(true, output);
	}

}
