package createCustTests;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestNonAlphaJUnit {

	@Test
	void test() {
		TestCustomer test = new TestCustomer();
		boolean output = test.testNonAlpha("John/Murphy");
		assertEquals(true, output);
	}

}
