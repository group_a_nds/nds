package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testNonNumJUnit {

	@Test
	void test() {
		TestCustomer test = new TestCustomer();
		boolean output = test.testNonNum("087f234567");
		assertEquals(true, output);
	}

}
