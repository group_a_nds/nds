package createCustTests;

public class TestCustomer {

	public boolean testAlphaChar(String name) {
		return name.matches("[a-z A-Z]+");		
	}
	
	public boolean testNonAlpha(String name){
		return !name.matches("[a-z A-Z]+");
	}
	
	public boolean testNonNum(String numChar) {
		return !numChar.matches("[0-9]+");
	}
	
	public boolean testNumTenChars(String ten) {
		 if(ten.length()==10) {
		 	return true;
		 	}
		return false;		 
	}
	
	public boolean testEmailFormat(String email) {
		if(email.contains("@.")) {
			return true;
			}
		return false;
	}
	
	public boolean testNumPositive(int positive) {
		if(positive>=0) {
			return true;
			}
		return false;
	}
	
}
