package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

class TestNumTenCharsJUnit {

	@Test
	void test() {
		TestCustomer test = new TestCustomer();
		boolean output = test.testNumTenChars("0851234567");
		assertEquals(true, output);
	}

}
