package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ndsApplication.*;
class createCustTestJunit {

	@Test
	void testCreateCustObj() {//test to confirm createCust method 
		Customer test = new Customer("john", "10 main st", "0871234567", "john@mail.com", "5");
		//testing each variable
		assertEquals("john",test.getName());
		assertEquals("10 main st",test.getAddress());
		assertEquals("0871234567",test.getPhone());
		assertEquals("john@mail.com",test.getEmail());
		assertEquals(5,test.getAreaCode());
		
	}

}
