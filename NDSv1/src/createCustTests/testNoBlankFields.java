package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.JTextField;

import org.junit.jupiter.api.Test;

import ndsApplication.NewsAgentWindow;

class testNoBlankFields {

	@Test
	void testvalidateField() {
		NewsAgentWindow test = new NewsAgentWindow();
		JTextField f = new JTextField("cust name");
		
		boolean result = test.validateField(f, "Please enter a name");
		
		assertEquals(true, result);
		
	}
	
	@Test
	void testvalidateInteger() {
		NewsAgentWindow test = new NewsAgentWindow();
		JTextField f = new JTextField("0877654321");
		
		boolean result = test.validateInteger(f, "Please enter phone number");
		
		assertEquals(true, result);
	}
	
	@Test
	void testfailedMessage() {
		NewsAgentWindow test = new NewsAgentWindow();
		JTextField f = new JTextField("0877654321");
		
		boolean result = test.failedMessage(f, "Please enter phone number");
		assertEquals(false, result);
	}

}
