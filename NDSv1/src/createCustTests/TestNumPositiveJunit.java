package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestNumPositiveJunit {

	@Test
	void test() {
		TestCustomer test = new TestCustomer();
		boolean output = test.testNumPositive(2);
		assertEquals(true, output);
	}

}
