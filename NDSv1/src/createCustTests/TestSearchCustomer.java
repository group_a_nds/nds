package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import ndsApplication.Customer;
import ndsApplication.DatabaseHandler;

class TestSearchCustomer {

	
	/* this first test will check for a name by inputting only part of 
	 * the name, ie the first name
	 * this test should return null as the search will only allow for for full 
	 * name search
	 */
	@Test
	void testPartName() {
		DatabaseHandler test = new DatabaseHandler();
		try {
			Customer result = test.searchCustByName("Bob");
			
			assertEquals(null, result.getName());	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	/* the second test will test that the returned value is null when the name searched is not 
	 * in the database
	 */
	@Test
	void testInvalidName() {
		DatabaseHandler test = new DatabaseHandler();
		try {
			Customer result = test.searchCustByName("Joe Dolan");
			
			assertEquals(null, result.getName());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	/*
	 * the final test on this method will test that for a valid name
	 *  if a name that is in the database is entered it should return
	 *  correct details
	 */
	@Test
	void testValid() {
		DatabaseHandler test = new DatabaseHandler();
		try {
			Customer result = test.searchCustByName("Bob Dylan");
			
			assertEquals("Bob Dylan", result.getName());
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}