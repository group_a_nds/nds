package createCustTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testEmailFormatJUnit {

	@Test
	void test() {
		TestCustomer test = new TestCustomer();
		boolean output = test.testEmailFormat("john@.com");
		assertEquals(true, output);
	}

}
